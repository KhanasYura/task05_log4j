package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main class.
 */
public class Application {
    /**
     * Logger.
     */
    private static Logger logger = LogManager.getLogger();

    /**
     * Constructor
     */
    private void Application() {
    }

    /**
     * Start point.
     *
     * @param args default parameters
     */
    public static void main(final String[] args) {

        new Parrot();
        new Cow();

        logger.trace("Exception");
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is an fatal message");
    }
}
