package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Parrot class.
 */
public class Parrot {
    /**
     * Logger.
     */
    private static Logger logger = LogManager.getLogger();

    /**
     * Constructor.
     */
    public Parrot() {
        logger.warn("Parrot This is a warn message");
        logger.error("Parrot This is an error message");
        logger.fatal("Parrot This is an fatal message");
    }
}
