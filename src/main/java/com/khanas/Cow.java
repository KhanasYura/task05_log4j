package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Cow class.
 */
public class Cow {
    /**
     * Logger.
     */
    private static Logger logger = LogManager.getLogger();

    /**
     * Constructor.
     */
    public Cow() {
        logger.warn("Cow This is a warn message");
        logger.error("Cow This is an error message");
        logger.fatal("Cow This is an fatal message");
    }
}
