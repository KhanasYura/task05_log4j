package com.khanas;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * ExampleSMS.
 */
public class ExampleSMS {
    /**
     * Account info.
     */
    public static final String ACCOUNT_SID = "AC783490bc329fc3ded520ef9d3fc89e6a";
    /**
     * Account info.
     */
    public static final String AUTH_TOKEN = "130cae9929881d93e1e05816b7bae56b";

    /**
     * Sender.
     */
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380990095274"), new PhoneNumber("+12055764173"), str).create();
    }

}
